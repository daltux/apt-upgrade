# apt-upgrade

Shell script for easier upgrading a [Debian](https://debian.org)-based system
using **`apt`** (or `nala`) and also, when present, `snap`, `flatpak` and `fwupdmgr`.

## Dependencies

### Mandatory dependencies
- [APT](https://en.wikipedia.org/wiki/APT_\(software\)) ([Debian](https://debian.org) or derivatives)
- [GNU Bash](https://gnu.org/software/bash/) (package [`bash`](https://packages.debian.org/stable/bash))
- [GNU Coreutils](http://gnu.org/software/coreutils/) ([`coreutils`](https://packages.debian.org/stable/coreutils))

### Optional dependencies
- [`sudo`](https://packages.debian.org/stable/sudo)
- [`nala`](https://packages.debian.org/stable/nala)
- [`snapd`](https://packages.debian.org/stable/snapd)
- [`flatpak`](https://packages.debian.org/stable/flatpak)
- [`fwupd`](https://packages.debian.org/stable/fwupd)
- from [`util-linux`](https://packages.debian.org/stable/util-linux):
	- [`ionice`](https://manpages.debian.org/stable/util-linux/ionice.1)
	for running the tool with lowest I/O priority.
	- [`hardlink`](https://manpages.debian.org/stable/util-linux/hardlink.1)
	for linking identical files in APT lists directory, saving space.
		- Until Debian 11 "bullseye" (or [Ubuntu 20.04](https://packages.ubuntu.com/focal/hardlink)),
		this was a different program, from package [`hardlink`](https://packages.debian.org/bullseye/hardlink)
        ([manpage](https://manpages.debian.org/bullseye/hardlink/hardlink.1.en.html)),
		less tested here.

## Usage

You can download or clone this repository and make a symbolic link to the
**`apt-upgrade`** script somewhere in your `PATH`, e.g. "`~/bin/`" or
"`/usr/local/bin/`". Then you can run it as root or, if you use `sudo`, even as
an unpriviledged user, as it will call `sudo` to proceed, if needed.

Please check all possible parameters:

```
apt-upgrade --help
```

If you would like to always use some parameters, an idea is setting a persistent
alias. Look at your shell documentation on how to do this. On Bash it would be
e.g. in `~/.bash_aliases`.

## Copyright, licensing and disclaimer

[<img align="right" alt="GPLv3 - Free Software - Free as in Freedom" title="GNU
General Public License, version 3" src="https://www.gnu.org/graphics/gplv3-with-text-136x68.png">](LICENSE.md)

Copyright © 2023, 2024, 2025 [Daltux](https://daltux.net)

`apt-upgrade` is [Free Software](https://gnu.org/philosophy/free-sw.html):
you can redistribute it and/or modify it under the terms of the [GNU General
Public License](LICENSE.md) published by the [Free Software
Foundation](https://fsf.org), either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the [GNU General Public License](LICENSE.md) for more
details.

