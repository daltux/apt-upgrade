#!/bin/bash

# apt-upgrade: easier upgrade the system using apt (or nala when
# present) and, also when present, snap, flatpak and fwupdmgr.
# 
# Copyright © 2023, 2024, 2025 Daltux <https://daltux.net>
# 
# This program is Free Software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

FORCE_IPV4="-o Acquire::ForceIPv4=true"
ORIG_UID=${ORIG_UID:-$UID}
PARAMETERS="${*:-\"\"}"

[[ "$PARAMETERS" == *'-v'* ]] && echo "Script called with parameters: $PARAMETERS"

if [ -z "$MY_USER" ] ; then
	[[ "$PARAMETERS" == *'-v'* ]] && echo "ORIG_UID=$ORIG_UID"

	if [ "$ORIG_UID" -ne 0 ] ; then
		MY_USER=$(logname 2> /dev/null)
		[ -z "$MY_USER" ] && MY_USER=$(who -m | awk '{print $1;}')
	fi
fi

export FORCE_IPV4 ORIG_UID PARAMETERS MY_USER

check_apt () {
	APT_CMD="${APT_CMD:-apt}"

	if command -v nala > /dev/null 2>&1 ; then
		if [[ "$PARAMETERS" == *'-nn'* ]] || [[ "$PARAMETERS" == *'--no-nala'* ]] ; then
			echo 'As requested, not using "nala", then actual "apt" will be used.'
			echo
		else
			APT_CMD=nala
		fi
	fi
	
	[[ "$PARAMETERS" == *'-ou'* ]] || [[ "$PARAMETERS" == *'--only-update'* ]]
	UPDATE_ONLY=$?

	[[ "$PARAMETERS" == *'-nu'* ]] || [[ "$PARAMETERS" == *'--no-update'* ]]
	SKIP_UPDATE=$?
	
	if [ "$UPDATE_ONLY" -eq 0 ] && [ "$SKIP_UPDATE" -eq 0 ] ; then
		echo 'Invalid arguments: -ou or --only-update incompatible with -nu or --no-update'
		exit 10
	fi

	if [ "$SKIP_UPDATE" -eq 0 ] ; then
		echo "Skipping $APT_CMD update as requested."
	else
		[[ "$APT_CMD" == 'nala' ]] && UPD_PARAMS="$UPD_PARAMS -v"
		
		UPD_MSG="* Running $APT_CMD update"
		
		if [[ "$PARAMETERS" == *'-4'* ]] ; then
			UPD_MSG="$UPD_MSG on IPv4 only"
			UPD_PARAMS="$UPD_PARAMS $FORCE_IPV4"
		fi
		
		echo "$UPD_MSG ..."
		echo
		# shellcheck disable=SC2086
		"$APT_CMD" update $UPD_PARAMS
	fi
	
	UPDATE_OK=$?
	echo

	if [ "$UPDATE_OK" -ne 0 ] ; then
		return "$UPDATE_OK"
	fi

	if [ "$SKIP_UPDATE" -eq 0 ] || [[ "${PARAMETERS}" == *'--no-hardlink'* ]] ; then
		echo Skipping hardlink as requested.
	else
		APT_LISTS_DIR='/var/lib/apt/lists'
		if command -v hardlink > /dev/null 2>&1 && [ -e "$APT_LISTS_DIR" ] ; then
			echo "* Linking identical files in APT lists directory \"$APT_LISTS_DIR\" ..."
			echo
			hardlink --ignore-time --respect-xattrs "$APT_LISTS_DIR"
			# --reflink=auto is available only on util-linux' hardlink
		else
			echo "WARNING: there is no command \"hardlink\" or directory \"$APT_LISTS_DIR\"."
		fi
	fi

	[[ "$PARAMETERS" == *'--full'* ]] && APT_ACTION='full-upgrade' || APT_ACTION='upgrade'
	
	case "$APT_CMD" in
		apt)
			APT_PARAMS='--verbose-versions'
			;;
		nala)
			if [ "$APT_ACTION" = 'full-upgrade' ] ; then
				APT_ACTION='upgrade'
				APT_PARAMS='--full'
			else
				APT_PARAMS='--no-full'
			fi
			
			APT_PARAMS="-v --no-update $APT_PARAMS"
			;;
		*)
			echo "Warning: unexpected APT_CMD \"$APT_CMD\""
			;;
	esac

	echo
	echo "$FREE_MSG $(free_space||true)"
	
	[[ "$PARAMETERS" == *'-4'* ]] && APT_PARAMS="$APT_PARAMS $FORCE_IPV4"
	[[ "$PARAMETERS" == *'-y'* ]] && APT_PARAMS="$APT_PARAMS -y"
	
	if [ "$UPDATE_ONLY" -eq 0 ] ; then
		echo "Skipping $APT_CMD $APT_ACTION as only an update was requested."
		return
	fi

	echo
	echo "* Running $APT_CMD $APT_ACTION $APT_PARAMS ..."
	echo

	# shellcheck disable=SC2086
	"$APT_CMD" "$APT_ACTION" $APT_PARAMS
	UPGRADE_OK=$?

	if [ $UPGRADE_OK -eq 0 ]; then
		echo
		echo "* Cleaning with $APT_CMD autoremove/purge and clean ..."
		echo
		$APT_CMD autoremove --purge && $APT_CMD clean
	else
		echo
		echo "*** $APT_CMD aborted or exited with error!"
		exit $UPGRADE_OK
	fi
}

check_snap () {
	echo

	if [[ "${PARAMETERS}" == *'--no-snap'* ]] ; then
		echo Skipping Snap as requested.
		echo
	else
		if command -v snap > /dev/null 2>&1 ; then
			echo -n '* Atualizando snaps... '
			snap refresh && echo
		else
			echo 'Skipping snap as it is apparently not installed.'
			echo
		fi
	fi
}

check_flatpak () {
	if [[ "$PARAMETERS" == *'--no-flatpak'* ]] ; then
		echo Skipping Flatpak as requested.
		echo
	else
		if command -v flatpak > /dev/null 2>&1 ; then
			echo '* System Flatpak: '
			flatpak --system uninstall --unused -y && echo
			flatpak --system update -y && echo
			
			if [ -n "$MY_USER" ] ; then
				echo "* User ${MY_USER}'s Flatpak: "
				runuser -u "$MY_USER" -- flatpak --user uninstall --unused -y && echo
				runuser -u "$MY_USER" -- flatpak --user update -y && echo
			elif [ "$ORIG_UID" -ne 0 ] ; then
				echo "MY_USER undefined, so we will not update their Flatpak."
				echo
			fi
		else
			echo Skipping flatpak as it is apparently not installed.
			echo
		fi
	fi
}

check_fwupd () {
	if [[ "${PARAMETERS}" == *'--no-firmware'* ]] ; then
		echo Skipping firmware update as requested.
		echo
	else
		if command -v fwupdmgr > /dev/null 2>&1 ; then
			echo '* Updating firmware with fwupdmgr... '
			echo
			fwupdmgr refresh
			#fwupdmgr get-updates
			fwupdmgr update && echo
		else
			echo 'Skipping fwupdmgr as it is apparently not installed.'
			echo
		fi
	fi
}

### Flash is deprecated
#flashToFirefoxSnap () {
#	DEST_DIR="/home/${MY_USER}/snap/firefox/common/.mozilla/plugins/"
#	
#	if [ -e "$DEST_DIR" ]; then
#		echo; echo "Firefox snap directory found. Checking if flash can be updatedo..."
#		
#		LIBFLASH_FILENAME="libflashplayer.so"
#		LIBFLASH_CHECKLIST=( "/usr/lib/adobe-flashplugin/$LIBFLASH_FILENAME" "/usr/lib/mozilla/plugins/$LIBFLASH_FILENAME" "/etc/alternatives/mozilla-flashplugin" )
#		
#		for origem in "${LIBFLASH_CHECKLIST[@]}"; do
#			if [ -s "$orig" ]; then
#				echo "Flash file found at $orig"
#				FOUND=true
#				cp --update --dereference --preserve=timestamps --verbose "$orig" "$DEST_DIR" && echo "OK."
#				break
#			fi
#		done
#
#		if ! $FOUND ; then
#			echo "Flash was not found for copying."
#		fi
#	fi
#}

free_space () {
	df --sync --output=avail -h / | tail -1
}

startup () {
	UPTIME_BEGIN=$(uptime)
	DF_BEGIN=$(free_space)
	echo
	check_apt
	apt_ok="$?"

	[ "$apt_ok" -eq 0 ] && { check_snap ; check_flatpak ; check_fwupd ; }
	#flashToFirefoxSnap # deprecated
	
	echo "Start:$UPTIME_BEGIN"
	echo "  end:$(uptime)"
	echo
	echo "$FREE_MSG"
	echo "$DF_BEGIN before"
	echo "$(free_space||true) after"

	exit "$apt_ok"
}

if [[ "$PARAMETERS" == *'--help'* ]] ; then
	echo "Updates the system: apt [or nala if present] (update/upgrade/autoremove/clean), snap, flatpak and fwupdmgr (if present)."
	echo
	echo "Possible parameters:"
	echo "	[ -4 ]  . . . . . . . apt will use only IPv4." # TAB at string start
	echo "	[ -ou | --only-update ] apt will only update repository data, skipping upgrade."
	echo "	[ --full ]  . . . . . Do a full-upgrade: upgrades and also removes packages if necessary. Very dangerous with -y!"
	echo "	[ -nn | --no-nala ] . Do not use nala even when it is present."
	echo "	[ -nu | --no-update ] Do not update apt repositories contents (does not run \"apt update\")."
	echo "	                      Implies --no-hardlink."
	echo "	[ --no-hardlink ] . . Do not use \"hardlink\" to save space in the APT lists directory."
	echo "	[ --no-firmware ] . . Do not update firmware."
	echo "	[ --no-flatpak ]  . . Do not update flatpak."
	echo "	[ --no-snap ] . . . . Do not update snap."
	echo "	[ -y ]  . . . . . . . Send -y to apt/nala upgrade, avoiding confirmations: \"apt full-upgrade -y\"."
	echo "	[ -v ]  . . . . . . . Display parameters used along running the script."
	echo "	[ -vv ] . . . . . . . Also use \"bash -x\" to run the script."
	echo "	[ --help ]  . . . . . Display this help text and exits."
else
	export FREE_MSG='Root file system free space:'
	startup
fi

