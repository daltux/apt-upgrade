#!/bin/bash

# apt-upgrade: script for easier upgrading the system using apt (or nala when
# present) and, also when present, snap, flatpak and fwupdmgr
# 
# Copyright © 2023, 2024 Daltux <https://daltux.net>
# 
# This program is Free Software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
	DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
	SOURCE="$(readlink "$SOURCE")"
	[[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

CMD='nice -n 19'
[[ "$*" == *'-vv'* ]] && CMD="${CMD} bash -x"
CMD="${CMD} \"$DIR/sub_apt-upgrade.sh\" \"$*\""

if type ionice > /dev/null 2>&1 ; then
	CMD="ionice -c3 $CMD"
fi

if type sudo > /dev/null 2>&1 && [ "$UID" -ne 0 ] ; then
	sudo -s ORIG_UID="$UID" eval "$CMD"
else
	eval "$CMD"
fi

